package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import domain.Contract;

public final class thankYou_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.Vector _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html; charset=ISO-8859-1");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\r\n");
      out.write("<html>\r\n");
      out.write("<head>\r\n");
      out.write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=ISO-8859-1\">\r\n");
      out.write("<title>Insert title here</title>\r\n");
      out.write("</head>\r\n");
      out.write("<body>\r\n");
      out.write("\r\n");
      domain.Contract contract = null;
      synchronized (application) {
        contract = (domain.Contract) _jspx_page_context.getAttribute("contract", PageContext.APPLICATION_SCOPE);
        if (contract == null){
          contract = new domain.Contract();
          _jspx_page_context.setAttribute("contract", contract, PageContext.APPLICATION_SCOPE);
        }
      }
      out.write('\r');
      out.write('\n');
      org.apache.jasper.runtime.JspRuntimeLibrary.introspecthelper(_jspx_page_context.findAttribute("contract"), "contractKind", request.getParameter("contractKind"), request, "contractKind", false);
      out.write('\r');
      out.write('\n');
      org.apache.jasper.runtime.JspRuntimeLibrary.introspecthelper(_jspx_page_context.findAttribute("contract"), "year", request.getParameter("year"), request, "year", false);
      out.write('\r');
      out.write('\n');
      org.apache.jasper.runtime.JspRuntimeLibrary.introspecthelper(_jspx_page_context.findAttribute("contract"), "quotaType", request.getParameter("quotaType"), request, "quotaType", false);
      out.write('\r');
      out.write('\n');
      org.apache.jasper.runtime.JspRuntimeLibrary.introspecthelper(_jspx_page_context.findAttribute("contract"), "amount", request.getParameter("amount"), request, "amount", false);
      out.write("\r\n");
      out.write("\r\n");
      out.write("contractKind ");
      out.write(org.apache.jasper.runtime.JspRuntimeLibrary.toString((((domain.Contract)_jspx_page_context.findAttribute("contract")).getContractKind())));
      out.write("<br>\r\n");
      out.write("year ");
      out.write(org.apache.jasper.runtime.JspRuntimeLibrary.toString((((domain.Contract)_jspx_page_context.findAttribute("contract")).getYear())));
      out.write("<br>\r\n");
      out.write("quotaType ");
      out.write(org.apache.jasper.runtime.JspRuntimeLibrary.toString((((domain.Contract)_jspx_page_context.findAttribute("contract")).getQuotaType())));
      out.write("<br>\r\n");
      out.write("amount ");
      out.write(org.apache.jasper.runtime.JspRuntimeLibrary.toString((((domain.Contract)_jspx_page_context.findAttribute("contract")).getAmount())));
      out.write("<br>\r\n");
      out.write("\r\n");
      out.write("\t<form action=\"pracaSuccess.jsp\">\r\n");
      out.write("\t\t<input type=\"submit\" value=\"nastepny krok\"/><br>\r\n");
      out.write("\t</form>\r\n");
      out.write("\r\n");

if(contract.getContractKind()=="praca"){

      out.write("\r\n");
      out.write("\t<form action=\"pracaSuccess.jsp\">\r\n");
      out.write("\t\t<input type=\"submit\" value=\"nastepny krok\"/><br>\r\n");
      out.write("\t</form>\r\n");

}

      out.write('\r');
      out.write('\n');

if(contract.getContractKind()=="dzielo"){

      out.write("\r\n");
      out.write("\t<form action=\"dzieloAddInfo.jsp\">\r\n");
      out.write("\t\t<input type=\"submit\" value=\"nastepny krok\"/><br>\r\n");
      out.write("\t</form>\r\n");

}

      out.write('\r');
      out.write('\n');

if(contract.getContractKind()=="zlecenie"){

      out.write("\r\n");
      out.write("\t<form action=\"zlecenieAddInfo.jsp\">\r\n");
      out.write("\t\t<input type=\"submit\" value=\"nastepny krok\"/><br>\r\n");
      out.write("\t</form>\r\n");

}

      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("</body>\r\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
