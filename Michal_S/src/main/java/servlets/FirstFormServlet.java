package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/firstForm")
public class FirstFormServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	@Override
	public void init() throws ServletException {
	}
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		out.println("<html><body><h2><strong>Witaj na stronie kalkulatora plac</strong><br></h2>"
				+ "<form action='main'>"
				+ "<label>Rodzaj umowy: </label><br>"
				+ "<select type='text' name='contractKind' multiple> <br>"
					+ "<option value='praca'>Umowa o prace<br>"
					+ "<option value='dzielo'>Umowa o dzielo<br>"
				+ "<option value='zlecenie'>Umowa zlecenie<br>"
				+ "</select><br>"
					+ "<label>Rok: <input type='number' id='year' name='year' value='2016'/></label><br>"
					+ "<label>Wybierz kwote netto lub brutto: </label><br>"
				+ "<select type='text' name='quotaType' multiple> <br>"
				+ "<option value='netto'>netto<br>"
				+ "<option value='brutto'>brutto<br>"
				+ "</select><br>"
				+ "<label>Kwota wynagrodzenia: <input type='number' id='amount' name='amount'/></label><br>"
				+ "<input type='submit' value='nastepny krok'/><br>"
			+ "</form>"
			+"</body></html>");
		out.close();
	}
}