package servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import calculator.CountDzieloBrutto;
import calculator.CountDzieloNetto;

@WebServlet("/dzieloSuccess")
public class DzieloSuccessServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		response.setContentType("text/html");
//		ServletContext context = getServletContext();
//		context.setAttribute("logged", false);
		PrintWriter out = response.getWriter();
		
		String kosztyUzyskaniaPrzychodu = request.getParameter("kosztyUzyskaniaPrzychodu");
		
		if(request.getSession().getAttribute("quotaType").equals("brutto")){
			
			CountDzieloBrutto count= new CountDzieloBrutto(Double.parseDouble((String) request.getSession().getAttribute("amount")),kosztyUzyskaniaPrzychodu);
			out.println("<html><body><h2>Oto wyniki!</h2>" +
					"Tabela dla umowy typu "+request.getSession().getAttribute("contractKind")+"!<br>"+
					"dla roku "+request.getSession().getAttribute("year")+"!<br>"+
					"<table border='1'>"+
					"<tr><td>Brutto</td><td>"+
					"Koszt uzyskania przychodu</td><td>Podstawa opodatkowania</td><td>"+
					"Zaliczka na PIT</td><td>Netto</td></tr>");
	
			out.println("<tr><td>" + count.getBrutto() +"</td><td>"+count.getKosztUzPrzy()+"</td><td>"+count.getPodstawa()+"</td><td>"+count.getZaliczkaPITdoZaplaty()+"</td><td>"+count.getNetto()+"</td></tr>");
			out.println("</table>"+
				"</body></html>");
			out.close();
		}
		else{
			CountDzieloNetto count= new CountDzieloNetto(Double.parseDouble((String) request.getSession().getAttribute("amount")),kosztyUzyskaniaPrzychodu);
			out.println("<html><body><h2>Oto wyniki!</h2>" +
					"Tabela dla umowy typu "+request.getSession().getAttribute("contractKind")+"!<br>"+
					"dla roku "+request.getSession().getAttribute("year")+"!<br>"+
					"<table border='1'>"+
					"<tr><td>Brutto</td><td>"+
					"Koszt uzyskania przychodu</td><td>Podstawa opodatkowania</td><td>"+
					"Zaliczka na PIT</td><td>Netto</td></tr>");
	
			out.println("<tr><td>" + count.getBrutto() +"</td><td>"+count.getKosztUzPrzy()+"</td><td>"+count.getPodstawa()+"</td><td>"+count.getZaliczkaPITdoZaplaty()+"</td><td>"+count.getNetto()+"</td></tr>");
			out.println("</table>"+
				"</body></html>");
			out.close();
		}
	}
}